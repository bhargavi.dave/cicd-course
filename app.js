const express = require('express')
const app = express()
const port = 8080
app.get('/', (req,res)=>{
    res.send('This is my node application for CICD Demo!')
})

app.listen(port,()=>{
    console.log(`Application is listening at http://localhost:${port}`)
})